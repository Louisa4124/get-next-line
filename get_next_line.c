/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/07 11:05:56 by lboudjem          #+#    #+#             */
/*   Updated: 2022/12/07 14:26:21 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	checkerror(int fd, char **str, char **line)
{
	if (line == NULL)
		return (-1);
	if (!*str)
	{
		*str = (char *)malloc(sizeof(char) * (BUFFER_SIZE + 1));
		if (!(*str))
			return (-1);
	}
	return (0);
}

static char	*readline(char *str, int fd)
{
	char	buff[BUFFER_SIZE + 1];
	int		ret;

	ret = read(fd, buff, BUFFER_SIZE);
	while (ret > 0)
	{
		buff[ret] = '\0';
		str = ft_strjoin(str, buff);
	}
	return (str);
}

char	*get_next_line(int fd)
{
	static char	*str;
}
